/*jslint node:true */
"use strict";

exports.register = function (options) {
    require('./home-page').register(options);
    require('./full-name').register(options);
    require('./crud-list').register(options);
    require('./local-sum').register(options);
    require('./remote-sum').register(options);
};
