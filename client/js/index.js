/*jslint node:true */
"use strict";

var ko = require('knockout'),
    components = require('./components'),
    createRepositories = require('./repositories').createRepositories;

components.register();
var repositories = createRepositories();

function App() {
    this.routes = {
        '/': 'home-page',
        '/fullname': 'full-name',
        '/crud': 'crud-list',
        '/local-sum': 'local-sum',
        '/remote-sum': 'remote-sum',
    };
    this.repositories = repositories;
}

ko.applyBindings(new App());
